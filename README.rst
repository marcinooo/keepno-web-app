======
Keepno
======

:Authors: Marcin Wachacki
:Tags: Python, Flask, web app, keepno

:abstract:

    The repository contains the source code of the Keepno application. Keepno is a web platform that is used to create notes. Each user can keep private notes for free. The code is a good example of the structure of a Flask application.
